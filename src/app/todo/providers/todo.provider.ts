import { BaseProvider } from 'src/app/shared/providers/provider.base';
import { Injectable } from '@angular/core';
import { HttpHelper } from 'src/app/shared/helpers/http.helper';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { ResponseGetAllTodosModel } from '../models/response-get-all-todos-todo.model';
import { RequestCreateTodoModel } from '../models/request-create-todo-todo.model';
import { RequestUpdateTodoModel } from '../models/request-update-todo-todo.model';
import { ResponseGetTodoModel } from '../models/response-get-todo-todo.model';

@Injectable({
    providedIn: 'root'
})
export class TodoProvider extends BaseProvider {

    constructor(httpHelper: HttpHelper, private readonly dialogService: DialogService) {
        super(httpHelper);
        this.baseUrl = `${this.baseUrl}/todo/`;
    }

    public async getAll(): Promise<ResponseGetAllTodosModel> {
        const todoList = await this.httpHelper.get<ResponseGetAllTodosModel>(this.baseUrl + 'getAll');
        return todoList;
    }

    public async create(requestCreateTodoModel: RequestCreateTodoModel): Promise<void> {
        await this.httpHelper.post<ResponseGetAllTodosModel>(this.baseUrl + 'create', requestCreateTodoModel);
    }

    public async update(requestUpdateTodoModel: RequestUpdateTodoModel): Promise<void> {
        await this.httpHelper.post<ResponseGetAllTodosModel>(this.baseUrl + 'update', requestUpdateTodoModel);
    }

    public async getById(id: string): Promise<ResponseGetTodoModel> {
        const todoItem = await this.httpHelper.get<ResponseGetTodoModel>(this.baseUrl + 'getById/' + id);
        return todoItem;
    }

    public async delete(id: string): Promise<void> {
        await this.httpHelper.get<void>(this.baseUrl + 'delete/' + id);
    }
}