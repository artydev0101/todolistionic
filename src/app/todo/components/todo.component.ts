import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from 'src/app/map/models/location.model';
import { Router } from '@angular/router';
import { TodoProvider } from 'src/app/todo/providers/todo.provider';
import * as constants from 'src/app/shared/constants/application.constants';

export class TodoComponent {

    protected image: string = constants.defaultImage;

    protected location: Location;

    protected todoForm: FormGroup;

    constructor(
        protected readonly formBuilder: FormBuilder,
        protected readonly domSanitizer: DomSanitizer,
        protected readonly router: Router,
        protected readonly todoProvider: TodoProvider) {
    }

    imagePicked($event) {
        if ($event) {
            this.image = $event.split(',')[1];
        }
    }

    clearImage() {
        this.image = constants.defaultImage;
    }

    isImageDefault() {
        return this.image == constants.defaultImage;
    }
}