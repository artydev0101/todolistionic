export class ResponseGetAllTodosModel{
    todoList: Array<ResponseTodoGetAllTodosModelItem>;
}

export class ResponseTodoGetAllTodosModelItem{
    id: string;
    title: string;
    isDone: boolean;
    image: string;
}