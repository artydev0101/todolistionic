import { Location } from 'src/app/map/models/location.model';

export class ResponseGetTodoModel {
    id: string;
    title: string;
    description: string;
    isDone: boolean;
    location: Location;
    image: string;
}