import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthenticationComponentBase } from 'src/app/identity/components/authentication.component.base';
import { AuthenticationProvider } from 'src/app/identity/providers/authentication.provider';
import { RequestSignInIdentityModel } from 'src/app/identity/models/request-sign-in-identity.model';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/identity/services/authentication.service';

@Component({
  selector: 'app-email-sign-in',
  templateUrl: './email-sign-in.component.html',
  styleUrls: ['./email-sign-in.component.scss'],
})
export class EmailSignInComponent extends AuthenticationComponentBase {

  constructor(
    formBuilder: FormBuilder,
    private readonly authenticationService: AuthenticationService) {
    super(formBuilder);
  }

  async onSubmit() {
    const requestSignInIdentityModel: RequestSignInIdentityModel = {
      email: this.authenticationForm.get('email').value,
      password: this.authenticationForm.get('password').value,
    }

    await this.authenticationService.signIn(requestSignInIdentityModel);
  }
}