import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInEmailPage } from './sign-in-email.page';

describe('SignInEmailPage', () => {
  let component: SignInEmailPage;
  let fixture: ComponentFixture<SignInEmailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInEmailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInEmailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
