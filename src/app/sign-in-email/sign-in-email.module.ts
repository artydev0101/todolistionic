import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignInEmailPage } from './sign-in-email.page';
import { EmailSignInComponent } from './email-sign-in/email-sign-in.component';

const routes: Routes = [
  {
    path: '',
    component: SignInEmailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignInEmailPage, EmailSignInComponent]
})
export class SignInEmailPageModule { }
