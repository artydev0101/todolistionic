import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ResponseGetTodoModel } from 'src/app/todo/models/response-get-todo-todo.model';
import { TodoProvider } from 'src/app/todo/providers/todo.provider';
import { TodoComponent } from 'src/app/todo/components/todo.component';
import { DialogService } from 'src/app/shared/services/dialog.service';

@Component({
  selector: 'app-update-todo',
  templateUrl: './update-todo.component.html',
  styleUrls: ['./update-todo.component.scss', '../../todo/styles/todo.scss'],
})
export class UpdateTodoComponent extends TodoComponent {

  private responseGetTodoModel: ResponseGetTodoModel;

  constructor(
    formBuilder: FormBuilder,
    domSanitizer: DomSanitizer,
    router: Router,
    todoProvider: TodoProvider,
    private readonly dialogService: DialogService
  ) {
    super(formBuilder, domSanitizer, router, todoProvider);
    this.todoForm = this.formBuilder.group({
      description: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      isDone: new FormControl('false')
    });
  }

  @Input() set todo(value: ResponseGetTodoModel) {
    this.responseGetTodoModel = value;

    if (this.responseGetTodoModel) {
      this.location = this.responseGetTodoModel.location;

      if (this.responseGetTodoModel.image) {
        this.image = this.responseGetTodoModel.image;
      }

      this.title = this.responseGetTodoModel.title;

      this.todoForm.setValue({
        description: this.responseGetTodoModel.description,
        isDone: this.responseGetTodoModel.isDone
      });
    }
  }

  get todo(): ResponseGetTodoModel {
    return this.responseGetTodoModel;
  }

  public title: string;

  public validation_messages = {
    'description': [
      { type: 'required', message: 'Description is required.' },
    ]
  };

  onSubmit() {
    this.todoProvider.update({
      id: this.responseGetTodoModel.id,
      description: this.todoForm.get('description').value,
      isDone: <boolean>this.todoForm.get('isDone').value,
      image: this.image.toString(),
    }).then(() => {
      this.router.navigate(['/tabs/todo-list', { isRefreshRequired: true }]);
    });
  }

  async delete() {
    const isDeletionConfirmed = await this.dialogService.showConfirmationDialog('Delete todo', 'Do you really want to delete this todo?')

    if (isDeletionConfirmed) {
      this.todoProvider.delete(this.responseGetTodoModel.id)
        .then(() => {
          this.router.navigate(['/tabs/todo-list', { isRefreshRequired: true }]);
        });
    }
  }
}