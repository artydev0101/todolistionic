import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResponseGetTodoModel } from '../todo/models/response-get-todo-todo.model';
import { DialogService } from '../shared/services/dialog.service';
import { TodoProvider } from '../todo/providers/todo.provider';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.page.html',
  styleUrls: ['./edit-todo.page.scss'],
})
export class EditTodoPage implements OnInit {

  todo: ResponseGetTodoModel;

  constructor(
    activatedRoute: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly todoProvider: TodoProvider
  ) {
    activatedRoute.params.subscribe(async (params) => {
      const todoId = params['todoId'];
      await this.loadTodo(todoId);
    });
  }

  ngOnInit() {
  }

  async getTodoById(id: string): Promise<ResponseGetTodoModel> {
    const todo = await this.todoProvider.getById(id);

    return todo;
  }

  private async loadTodo(id: string) {

    this.dialogService.showLoader('Todo is loading...');

    this.todo = await this.getTodoById(id);

    this.dialogService.hideLoader();
  }
}
