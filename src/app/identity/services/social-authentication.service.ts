import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationService } from 'src/app/identity/services/authentication.service';
import { OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

export class SocialAuthenticationService implements OnDestroy {

    private subscription: Subscription;

    constructor(
        protected readonly afAuth: AngularFireAuth,
        private readonly authenticationService: AuthenticationService,
    ) {
        this.subscription = this.afAuth.authState.subscribe(async (user) => {
            if (!user) {
                return;
            }

            await this.authenticationService.socialSignIn({ email: user.email });
        });
    }

    protected async signOut(): Promise<void> {
        await this.afAuth.auth.signOut();
        await this.authenticationService.signOut();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}