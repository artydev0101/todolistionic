import { Injectable } from '@angular/core';
import { SocialAuthenticationService } from './social-authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationService } from 'src/app/identity/services/authentication.service';
import { Facebook } from '@ionic-native/facebook/ngx';
import { DialogService } from 'src/app/shared/services/dialog.service';
import * as firebase from 'firebase/app';
import { ISocialAuthenticationService } from './interfaces/social-authentication.service.interface';


@Injectable({
    providedIn: 'root'
})
export class FacebookAuthenticationService extends SocialAuthenticationService implements ISocialAuthenticationService {

    constructor(
        private readonly facebook: Facebook,
        private readonly dialogService: DialogService,
        afAuth: AngularFireAuth,
        authenticationService: AuthenticationService) {
        super(afAuth, authenticationService)
    }

    public async login(): Promise<any> {        
        try {
            await this.signOut();

            const facebookLoginResponse = await this.facebook.login(['email']);

            const facebookCredential = firebase.auth.FacebookAuthProvider
                .credential(facebookLoginResponse.authResponse.accessToken);

            await firebase.auth().signInWithCredential(facebookCredential);
        } catch (error) {
            await this.dialogService.presentError(JSON.stringify(error));
        }
    }
}