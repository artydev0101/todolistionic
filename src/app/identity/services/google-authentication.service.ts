import { Injectable } from '@angular/core';
import { SocialAuthenticationService } from './social-authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationService } from 'src/app/identity/services/authentication.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform } from '@ionic/angular';
import { DialogService } from 'src/app/shared/services/dialog.service';
import * as firebase from 'firebase/app';
import { ISocialAuthenticationService } from './interfaces/social-authentication.service.interface';

@Injectable({
    providedIn: 'root'
})
export class GoogleAuthenticationService extends SocialAuthenticationService implements ISocialAuthenticationService {
    constructor(
        private readonly gplus: GooglePlus,
        private readonly platform: Platform,
        private readonly dialogService: DialogService,
        afAuth: AngularFireAuth,
        authenticationService: AuthenticationService,
    ) {
        super(afAuth, authenticationService)
    }

    public async login(): Promise<void> {        
        try {
            await this.signOut();

            if (this.platform.is('cordova')) {
                await this.nativeGoogleLogin();
            } else {
                await this.webGoogleLogin();
            }
        } catch (error) {
            this.dialogService.presentError(error);
        }
    }

    private async nativeGoogleLogin(): Promise<any> {
        const gplusUser = await this.gplus.login({
            'webClientId': '269346951475-4i7kdnumps4vj7rsn96b34rpf9u6nnk4.apps.googleusercontent.com',
            'offline': true,
            'scopes': 'profile email'
        });

        return await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken));
    }

    private async webGoogleLogin(): Promise<void> {
        const provider = new firebase.auth.GoogleAuthProvider();
        await this.afAuth.auth.signInWithPopup(provider);
    }
}