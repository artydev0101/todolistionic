export interface ISocialAuthenticationService {
    login(): Promise<any>;
}