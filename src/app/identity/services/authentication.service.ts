import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationProvider } from '../providers/authentication.provider';
import { RequestSignInIdentityModel, RequestSocialSignInIdentityModel } from '../models/request-sign-in-identity.model';
import { RequestSignUpIdentityModel } from '../models/request-sign-up-identity.model';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    authState = new BehaviorSubject(false);

    constructor(
        private readonly router: Router,
        private readonly storage: Storage,
        private readonly platform: Platform,
        public readonly toastController: ToastController,
        private readonly authenticationProvider: AuthenticationProvider
    ) {
        this.platform.ready().then(() => {
            this.isLoggedIn();
        });
    }

    isLoggedIn() {
        this.storage.get('token').then((response) => {
            if (response) {
                this.authState.next(true);
            }
        });
    }

    public async signIn(requestSignInIdentityModel: RequestSignInIdentityModel): Promise<void> {
        const signInResponse = await this.authenticationProvider.signIn(requestSignInIdentityModel);
        await this.completeSignIn(signInResponse.accessToken);
    }

    public async signUp(requestSignUpIdentityModel: RequestSignUpIdentityModel): Promise<void> {
        await this.authenticationProvider.signUp(requestSignUpIdentityModel);
        this.router.navigate(['sign-in']);
    }

    public async socialSignIn(requestSocialSignInIdentityModel: RequestSocialSignInIdentityModel): Promise<void> {
        const signInResponse = await this.authenticationProvider.socialSignIn(requestSocialSignInIdentityModel);
        await this.completeSignIn(signInResponse.accessToken);
    }

    private async completeSignIn(accessToken: string): Promise<void> {
        await this.storage.set('token', accessToken);
        this.router.navigate(['']);
        this.authState.next(true);
    }

    async signOut(): Promise<void> {
        await this.storage.remove('token');
        this.router.navigate(['sign-in']);
        this.authState.next(false);
    }

    isAuthenticated() {
        return this.authState.value;
    }
}