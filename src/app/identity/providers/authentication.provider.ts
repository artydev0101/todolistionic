import { Injectable } from '@angular/core';
import { HttpHelper } from '../../shared/helpers/http.helper';
import { BaseProvider } from '../../shared/providers/provider.base';
import { RequestSignInIdentityModel, RequestSocialSignInIdentityModel } from '../models/request-sign-in-identity.model';
import { RequestSignUpIdentityModel } from '../models/request-sign-up-identity.model';
import { DialogService } from '../../shared/services/dialog.service';
import { ResponseSignInIdentityModel } from '../models/response-sign-in-identity.model';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationProvider extends BaseProvider {

    constructor(httpHelper: HttpHelper) {
        super(httpHelper);
        this.baseUrl = `${this.baseUrl}/identity/`;
    }

    public async signIn(requestSignInIdentityModel: RequestSignInIdentityModel): Promise<ResponseSignInIdentityModel> {
        const responseSignInIdentityModel = await this.httpHelper.post<ResponseSignInIdentityModel>(this.baseUrl + 'sign-in', requestSignInIdentityModel);
        return responseSignInIdentityModel;
    }

    public async socialSignIn(requestSocialSignInIdentityModel: RequestSocialSignInIdentityModel): Promise<ResponseSignInIdentityModel> {
        const responseSignInIdentityModel = await this.httpHelper.post<ResponseSignInIdentityModel>(this.baseUrl + 'social-sign-in', requestSocialSignInIdentityModel);
        return responseSignInIdentityModel;
    }

    public async signUp(requestSignUpIdentityModel: RequestSignUpIdentityModel): Promise<void> {
        await this.httpHelper.post<void>(this.baseUrl + 'sign-up', requestSignUpIdentityModel);
    }
}