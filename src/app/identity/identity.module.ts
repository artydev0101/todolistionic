import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpHelper } from '../shared/helpers/http.helper';
import { DialogService } from '../shared/services/dialog.service';
import { BaseProvider } from '../shared/providers/provider.base';
import { AuthenticationService } from './services/authentication.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    HttpHelper,
    DialogService,
    AuthenticationService
  ]
})
export class IdentityModule { }
