import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { OnInit } from '@angular/core';

export class AuthenticationComponentBase implements OnInit {

    protected authenticationForm: FormGroup;

    protected validation_messages = {
        'email': [
            { type: 'required', message: 'Email is required.' },
            { type: 'pattern', message: 'Email is not valid.' },
        ],
        'password': [
            { type: 'required', message: 'Password is required.' },
            { type: 'minlength', message: 'Password must be at least 6 characters long.' },
            { type: 'pattern', message: 'Password must contain uppercase, lowercase symbols and numbers.' },
        ]
    };

    constructor(protected formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.authenticationForm = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
            ]))
        });
    }
}