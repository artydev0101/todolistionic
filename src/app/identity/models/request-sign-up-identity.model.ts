export class RequestSignUpIdentityModel {
    email: string;
    password: string;
    passwordConfirmation: string;
}