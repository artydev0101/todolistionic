export class RequestSignInIdentityModel {
    email: string;
    password: string;
}

export class RequestSocialSignInIdentityModel {
    email: string;
}