import { Component, OnInit } from '@angular/core';
import { ResponseTodoGetAllTodosModelItem } from '../todo/models/response-get-all-todos-todo.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TodoProvider } from '../todo/providers/todo.provider';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.page.html',
  styleUrls: ['./todo-list.page.scss'],
})
export class TodoListPage implements OnInit {

  public todos: Array<ResponseTodoGetAllTodosModelItem>;

  constructor(private readonly router: Router, private readonly todoProvider: TodoProvider, activatedRoute: ActivatedRoute) {
    activatedRoute.params.subscribe(async (params) => {
      const isRefreshRequired = <boolean>params['isRefreshRequired'];
      if (isRefreshRequired) {
        this.todos = undefined;
        await this.loadAllTodos();
      }
    });
  }

  public async ionViewWillEnter(): Promise<void> {
    await this.loadAllTodos();
  }

  private loadAllTodos(): Promise<void> {
    return this.todoProvider.getAll().then(response => {
      this.todos = response.todoList;
    });
  }

  ngOnInit() {
  }

  itemClicked(id: string) {
    this.router.navigate(
      ['/edit-todo',
        {
          todoId: id
        }
      ]
    );
  }

  async refresh(event) {
    await this.loadAllTodos();
    event.target.complete();
  }
}
