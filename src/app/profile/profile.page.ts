import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../identity/services/authentication.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    private readonly authenticationService: AuthenticationService,
 
  ) { }

  ngOnInit() {
  }

  public async signOut(): Promise<void> {
    await this.authenticationService.signOut();
  }

 
}
