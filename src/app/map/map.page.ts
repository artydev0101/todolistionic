import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Events } from '@ionic/angular';
import { HereMapComponent } from './here-map/here-map.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  @ViewChild("heremap")
  hereMapComponent: HereMapComponent;

  constructor(
    private readonly events: Events,
    private readonly navController: NavController) {
  }

  ngOnInit() {
  }

  useLocation() {
    const location = this.hereMapComponent.getSelectedLocation();
    this.events.publish('mapNavigationResult', location);
    this.navController.pop();
  }
}
