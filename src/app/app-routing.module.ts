import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'sign-in', loadChildren: './sign-in/sign-in.module#SignInPageModule' },
  { path: 'sign-in-email', loadChildren: './sign-in-email/sign-in-email.module#SignInEmailPageModule' },
  { path: 'sign-up-email', loadChildren: './sign-up-email/sign-up-email.module#SignUpEmailPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule', canActivate: [AuthGuard] },
  { path: 'todo-list', loadChildren: './todo-list/todo-list.module#TodoListPageModule', canActivate: [AuthGuard] },
  { path: 'map', loadChildren: './map/map.module#MapPageModule' },
  { path: 'create-todo', loadChildren: './create-todo/create-todo.module#CreateTodoPageModule' },
  { path: 'edit-todo', loadChildren: './edit-todo/edit-todo.module#EditTodoPageModule' },
  { path: 'create-todo', loadChildren: './create-todo/create-todo.module#CreateTodoPageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [
    AuthGuard
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
