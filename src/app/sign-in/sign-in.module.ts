import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignInPage } from './sign-in.page';
import { FacebookSignInComponent } from './facebook-sign-in/facebook-sign-in.component';
import { GoogleSignInComponent } from './google-sign-in/google-sign-in.component';

const routes: Routes = [
  {
    path: '',
    component: SignInPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignInPage, FacebookSignInComponent, GoogleSignInComponent]
})
export class SignInPageModule { }
