import { Component } from '@angular/core';
import { GoogleAuthenticationService } from '../../identity/services/google-authentication.service';

@Component({
  selector: 'app-google-sign-in',
  templateUrl: './google-sign-in.component.html',
  styleUrls: ['./google-sign-in.component.scss', '../sign-in.page.scss'],
})
export class GoogleSignInComponent {

  constructor(private readonly googleAuthenticationService: GoogleAuthenticationService) {
  }

  public async login(): Promise<any> {
    await this.googleAuthenticationService.login();
  }
}