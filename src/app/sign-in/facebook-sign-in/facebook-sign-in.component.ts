import { Component } from '@angular/core';
import { FacebookAuthenticationService } from '../../identity/services/facebook-authentication.service';

@Component({
  selector: 'app-facebook-sign-in',
  templateUrl: './facebook-sign-in.component.html',
  styleUrls: ['./facebook-sign-in.component.scss', '../sign-in.page.scss'],
})
export class FacebookSignInComponent {

  constructor(private readonly facebookAuthenticationService: FacebookAuthenticationService) {
  }

  public async login(): Promise<any> {
    await this.facebookAuthenticationService.login();
  }
}
