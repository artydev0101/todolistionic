import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.page.html',
  styleUrls: ['./create-todo.page.scss', '../todo/styles/todo.scss'],
})
export class CreateTodoPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
