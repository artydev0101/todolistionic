import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Location } from 'src/app/map/models/location.model';
import { Router } from '@angular/router';
import { Events } from '@ionic/angular';
import { TodoProvider } from 'src/app/todo/providers/todo.provider';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { TodoComponent } from 'src/app/todo/components/todo.component';

@Component({
  selector: 'app-new-todo',
  templateUrl: './new-todo.component.html',
  styleUrls: ['./new-todo.component.scss', '../../todo/styles/todo.scss'],
})
export class NewTodoComponent extends TodoComponent {

  private mapNavigationResultEventName = 'mapNavigationResult';

  public validation_messages = {
    'title': [
      { type: 'required', message: 'Title is required.' },
    ],
    'description': [
      { type: 'required', message: 'Description is required.' },
    ]
  };

  constructor(
    formBuilder: FormBuilder,
    domSanitizer: DomSanitizer,
    router: Router,
    todoProvider: TodoProvider,
    private readonly events: Events,
    private readonly dialogService: DialogService) {
    super(formBuilder, domSanitizer, router, todoProvider);
    this.todoForm = this.formBuilder.group({
      title: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      description: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      isDone: new FormControl(false)
    });
  }

  onSubmit() {
    this.todoProvider.create({
      title: this.todoForm.get('title').value,
      description: this.todoForm.get('description').value,
      isDone: this.todoForm.get('isDone').value,
      image: this.image,
      location: this.location
    }).then(() => {
      this.router.navigate(['/tabs/todo-list', { isRefreshRequired: true }]);
    });
  }

  pickLocation() {
    this.events.subscribe(this.mapNavigationResultEventName, (location) => {
      this.events.unsubscribe(this.mapNavigationResultEventName);
      this.location = location;
    });
    this.router.navigate(['/map']);
  }

  clearLocation() {
    this.location = undefined;
  }

  setLocation(location: Location) {
    this.location = location;
  }
}