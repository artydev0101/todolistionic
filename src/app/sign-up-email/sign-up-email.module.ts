import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignUpEmailPage } from './sign-up-email.page';
import { EmailSignUpComponent } from './email-sign-up/email-sign-up.component';

const routes: Routes = [
  {
    path: '',
    component: SignUpEmailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignUpEmailPage, EmailSignUpComponent]
})
export class SignUpEmailPageModule { }
