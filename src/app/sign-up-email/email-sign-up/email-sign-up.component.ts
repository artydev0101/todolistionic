import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticationComponentBase } from 'src/app/identity/components/authentication.component.base';
import { MustMatch } from '../../../app/shared/validators/match.validator';
import { RequestSignUpIdentityModel } from 'src/app/identity/models/request-sign-up-identity.model';
import { AuthenticationProvider } from 'src/app/identity/providers/authentication.provider';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/identity/services/authentication.service';

@Component({
  selector: 'app-email-sign-up',
  templateUrl: './email-sign-up.component.html',
  styleUrls: ['./email-sign-up.component.scss'],
})
export class EmailSignUpComponent extends AuthenticationComponentBase implements OnInit {

  constructor(
    formBuilder: FormBuilder,
    private readonly authenticationService: AuthenticationService) {
    super(formBuilder);
  }

  ngOnInit(): void {

    super.ngOnInit();

    this.authenticationForm.controls.passwordConfirmation = new FormControl('', Validators.required);

    this.authenticationForm = this.formBuilder.group(this.authenticationForm.controls, {
      validator: MustMatch('password', 'passwordConfirmation')
    });

    (this.validation_messages as any).passwordConfirmation = [
      { type: 'mustMatch', message: 'Password confirmation must match password.' },
    ];
  }

  onSubmit() {
    const requestSignInIdentityModel: RequestSignUpIdentityModel = {
      email: this.authenticationForm.get('email').value,
      password: this.authenticationForm.get('password').value,
      passwordConfirmation: this.authenticationForm.get('passwordConfirmation').value,
    }

    this.authenticationService.signUp(requestSignInIdentityModel);
  }
}
