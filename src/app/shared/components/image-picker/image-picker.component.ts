import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

enum PickerSourceType {
  PhotoLibrary = 0,
  Camera = 1,
  SavedPhotoAlbum = 2
}

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss'],
})
export class ImagePickerComponent implements OnInit {

  @Input() buttonTitle: string;
  @Input() imageWidth: number;
  @Input() imageHeight: number;

  @Output() imagePickedEvent = new EventEmitter<string>();

  private base64Prefix = 'data:Image/jpeg;base64,'

  constructor(
    private readonly actionSheetController: ActionSheetController,
    private readonly camera: Camera) { }

  ngOnInit() { }

  public async pickImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons: [
        {
          text: 'Pick From Gallery',
          handler: async () => {
            await this.openPicker(PickerSourceType.PhotoLibrary);
          }
        },
        {
          text: 'Take From Camera',
          handler: async () => {
            await this.openPicker(PickerSourceType.Camera);
          }
        },
        {
          text: 'Cancel',
          handler: () => {
            this.actionSheetController.dismiss();
          }
        }]
    });

    await actionSheet.present();
  }

  private openPicker(sourceType: number): Promise<void> {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: this.imageHeight,
      targetWidth: this.imageWidth,
      sourceType: sourceType
    }

    return this.camera.getPicture(options).then((image) => {
      const imageData = this.getImageData(image);
      this.imagePickedEvent.emit(imageData);
    }, (err) => {
      this.imagePickedEvent.emit(undefined);
    });
  }

  private getImageData(imageData: string): string {
    if (imageData) {
      imageData = this.base64Prefix + imageData;
    }
    return imageData;
  }
}
