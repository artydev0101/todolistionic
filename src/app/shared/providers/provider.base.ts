import { HttpHelper } from '../helpers/http.helper';

export class BaseProvider {

    protected baseUrl = 'http://a83c4429.ngrok.io';

    constructor(protected readonly httpHelper: HttpHelper) { }
}