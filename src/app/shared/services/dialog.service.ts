import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private readonly alertController: AlertController,
    private readonly loadingController: LoadingController
  ) { }

  async presentAlert(title: string, message: string) {

    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['Ok']
    });

    return await alert.present();
  }

  async presentError(message: string) {
    await this.presentAlert('Error', message);
  }

  showLoader(message: string) {
    this.loadingController.create({
      message: message
    }).then((loading) => {
      loading.present();
    });
  }

  showConfirmationDialog(title: string, message: string): Promise<boolean> {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        header: title,
        message: message,
        buttons: [
          {
            text: 'Yes',
            cssClass: 'secondary',
            handler: () => {
              resolve(true);
            }
          }, {
            text: 'No',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      await alert.present();
    });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }
}
