import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DialogService } from '../services/dialog.service';

@Injectable({
    providedIn: 'root'
})
export class HttpHelper {
    constructor(private readonly httpClient: HttpClient, protected readonly dialogService: DialogService) { }

    public async get<TResponse>(url: string): Promise<TResponse> {
        const response = await this.httpClient.get<TResponse>(url, { observe: 'response' }).toPromise();

        if (!this.isSuccessStatusCode(response.status)) {
            return;
        }

        return response.body;
    }

    public async post<TResponse>(url: string, body: any): Promise<TResponse> {
        const response = await this.httpClient.post<TResponse>(url, body, { observe: 'response' }).toPromise();

        if (!this.isSuccessStatusCode(response.status)) {
            return;
        }

        return response.body;
    }

    private isSuccessStatusCode(statusCode: number): boolean {
        return statusCode.toString().startsWith('2');
    }
}