import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagePickerComponent } from './components/image-picker/image-picker.component';
import { IdentityModule } from '../identity/identity.module';
import { LocationComponent } from './components/location/location.component';

@NgModule({
  declarations: [ImagePickerComponent, LocationComponent],
  imports: [
    CommonModule,
  ],
  exports: [
    ImagePickerComponent,
    LocationComponent,
    IdentityModule
  ]
})
export class SharedModule { }