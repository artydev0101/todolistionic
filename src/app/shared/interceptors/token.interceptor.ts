import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {
    Router
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DialogService } from '../services/dialog.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(
        private readonly router: Router,
        private readonly storage: Storage,
        private readonly dialogService: DialogService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(this.storage.get('token'))
            .pipe(
                switchMap(token => {
                    if (token) {
                        request = request.clone({
                            setHeaders: {
                                'Authorization': 'Bearer ' + token
                            }
                        });
                    }

                    if (!request.headers.has('Content-Type')) {
                        request = request.clone({
                            setHeaders: {
                                'content-type': 'application/json'
                            }
                        });
                    }

                    request = request.clone({
                        headers: request.headers.set('Accept', '*/*')
                    });

                    return next.handle(request).pipe(
                        map((event: HttpEvent<any>) => {
                            if (event instanceof HttpResponse) {
                                console.log('event--->>>', event);
                            }
                            return event;
                        }),
                        catchError((error: HttpErrorResponse) => {
                            if (error.status === 401) {                                
                                this.router.navigate(['sign-in']);
                            }

                            if (error.status === 400) {
                                var errorMessage = '';
                                error.error.message.forEach(message => {
                                    for (var constraint in message.constraints) {
                                        errorMessage += message.constraints[constraint] + '\n'
                                    }
                                });
                                this.dialogService.presentError(errorMessage);
                            }
                            return throwError(error);
                        }));
                })
            );
    }
}