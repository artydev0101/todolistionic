// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCha2oco6mBHpdstI41_E8wG_0w6rEg8A0",
    authDomain: "artydevtodolist.firebaseapp.com",
    databaseURL: "https://artydevtodolist.firebaseio.com",
    projectId: "artydevtodolist",
    storageBucket: "",
    messagingSenderId: "269346951475",
    appId: "1:269346951475:web:55b822ce061c9109"
  },
  hereMapApiKey: "8sjfrezW30I6uRV7jZf9bNhOZvQkp-ajXp5jJ3UuGGM",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
